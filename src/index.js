/* eslint-disable max-len */
const fs = require('fs');
const glob = require('glob');
const path = require('path');

const dirSdcard = '/Users/yrambler2001/Documents/PA/sdcard';
const dirToSave = '/Users/yrambler2001/Documents/PA/done';

const monthsArray = ['01_Січень', '02_Лютий', '03_Березень', '04_Квітень', '05_Травень', '06_Червень', '07_Липень', '08_Серпень', '09_Вересень', '10_Жовтень', '11_Листопад', '12_Грудень'];
const getMonthName = (monthString) => monthsArray[monthString - 1];
const findFiles = (dir, pattern) => new Promise((resolve, reject) => glob(pattern, { cwd: dir }, (error, files) => { if (error) reject(error); else resolve(files); }));

const App = async () => {
  const JPGs = (await findFiles(dirSdcard, path.join('**', 'IMG_????????_??????*.jpg'))).map((relativePath) => ({ filePath: path.join(dirSdcard, relativePath), fileName: path.basename(relativePath) }));
  JPGs.forEach(({ filePath, fileName }) => {
    const [year, month] = [fileName.substr(4, 4), getMonthName(fileName.substr(8, 2))];
    const outputFilePath = path.join(dirToSave, year, month, fileName);
    fs.mkdirSync(path.dirname(outputFilePath), { recursive: true });
    fs.renameSync(filePath, outputFilePath);
  });
};
App();
